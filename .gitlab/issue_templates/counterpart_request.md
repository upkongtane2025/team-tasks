<!---
Hello! The database group is excited to be able to help you deliver your awesome
new feature/fixed bug/technical debt to gitlab! In order to help us help you,
Please fill out answers to the sections below so that we can help prioritize 
and understand the work you need done/help with!
--->
## Request

### What kind of support are you looking for?

- [ ] [Feature Group Stable Counterpart](https://about.gitlab.com/handbook/engineering/development/enablement/database/stable.html#stable-counterparts)
- [ ] Dedicated team member/resource
- [ ] Advice on structure/performance/ect

### What team?

- Handbook: <!-- Link to a team page like https://about.gitlab.com/handbook/engineering/development/enablement/database -->
- Slack Channel: <!-- Slack channel used to talk like #g_database -->
- Engineering Manager: <!-- Who manages this team (gitlab handle) -->
- Label: <!-- team label like ~"group::database" -->

### Describe the feature or ongoing work that needs assistance

<!---
In here, Add details about:
- Links to an epic with information about the project
- Details about how critical the work is
--->

### Expectations for participating member(s) of the database group

<!---
You've already checked a box above, but please explicitly spell out
what you expect the database team member to do.
Good things to include: An SLO for responding to questions/reviews,
expectations about solo work vs pairing and collaboration, expected
end/delivery dates. That sort of thing.
--->

## Checklist

### Author
- [ ] The issue has a descriptive title
- [ ] There are detailed answers to the questions above
- [ ] The issue is assigned to the [database team manager](https://about.gitlab.com/handbook/engineering/development/enablement/database/#team-members)
- [ ] If this is urgent, reach out to the team manager in slack

### Manager
- [ ] There is enough information to prioritize the request
- [ ] The request has been assigned to a member of the team
- [ ] The priority of the request has been agreed by the stakeholders and author

/assign @alexives
/label ~"group::database" ~"devops::enablement" ~"database::active"
