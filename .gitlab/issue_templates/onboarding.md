This issue contains pointers and starting points for everything database
at GitLab.

### Database reviews and maintainer training

* [ ] Familiarize yourself with our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) and [Database Review Guidelines](https://docs.gitlab.com/ee/development/database_review.html#how-to-prepare-the-merge-request-for-a-database-review)
* [ ] Review the process of [How to become a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer)
* [ ] Create [Database Trainee Issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-database-maintainer) for yourself using template 'Trainee database maintainer'

### Chatops and Database Labs

* [ ] Join `#database-lab` on Slack and read about [Joe, the query optimization bot](https://docs.gitlab.com/ee/development/understanding_explain_plans.html#database-lab) (a more detailed background read can be found in [Blueprint](https://about.gitlab.com/handbook/engineering/infrastructure/library/database/postgres/query-optimization-bot/blueprint/) and [Design](https://about.gitlab.com/handbook/engineering/infrastructure/library/database/postgres/query-optimization-bot/design/))
* [ ] Set yourself up for [ChatOps](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html)
* [ ] Get a query plan for `SELECT * FROM users where username='$yourname'` or any other interesting query from ChatOps (`/chatops run explain $sql_query_here`) and `#database-lab` to make sure you're all set up
* [ ] In a `#database-lab` session, try dropping the index `index_users_on_username` and repeat above experiment to see how the plan changes.
* [ ] Ask one of your database group teammates to add you to [Postgres.ai](https://postgres.ai/)
  * [ ] Sign in with Google
  * [ ] Explore the [Postgres.ai documentation](https://postgres.ai/docs/)

### Groups, Slack and other communication

* [ ] Get yourself added to [`@gl-database`](https://gitlab.com/groups/gl-database) as a Developer. Reach out to any Maintainer of the group.
* [ ] Relevant Slack channels: `#database, #g_database, #backend, #production, #database-lab`
* [ ] Pro tip: Turn your [global notification settings](https://gitlab.com/-/profile/notifications) to "on mention only"
* [ ] Get yourself invited for Database Office Hours and add the [GitLab Team Meetings](https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar) calendar to your calendar
* [ ] Make sure your role description in the team YAML file includes `, Database` to add you to the [team page](https://about.gitlab.com/handbook/engineering/development/enablement/database/) - [example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/86185#note_623776780)

### GitLab.com database monitoring, logs

* [ ] Explore Grafana dashboards in general and around PostgreSQL, e.g. the [PostgreSQL Overview](https://dashboards.gitlab.net)
* [ ] Find GitLab.com logging in [Kibana](https://log.gitlab.net/)

### GitLab development

* [ ] Read through the [Database Guides](https://docs.gitlab.com/ee/development/README.html#database-guides) section, in particular [Migration Style Guide](https://docs.gitlab.com/ee/development/migration_style_guide.html), [Post Deployment Migrations](https://docs.gitlab.com/ee/development/post_deployment_migrations.html), [Background Migrations](https://docs.gitlab.com/ee/development/background_migrations.html)

### Database Team

* [ ] Ask one of your teammates to add you to the [Database Team Project](https://gitlab.com/gitlab-org/database-team)
* [ ] Find links to boards, issue trackers and other documentation on
  the [Database Team Page](https://about.gitlab.com/handbook/engineering/development/enablement/database/)
* [ ] Send an improvement MR to this issue template (perhaps with additional points for future members joining the team)

### Access Requests
* [ ] File an access-request: [New Issue to Request Access](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=&issuable_template=Individual_Bulk_Access_Request)
  * [ ]  For staging.gitlab.com server access, ability to SSH into `rails-console` and `db-console`
  * [ ]  For gitlab.com server access, ability to SSH into `rails-console` and `db-console`
  * [ ]  For access to `gitlab-restore` GCP project with permissions to Compute, including SSH login (OS login)
  * [ ]  For access to [`postgres-gprd` project](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/)

### Other

* [ ]  Review info on [bastions](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/granting-rails-or-db-access.md) and how to access dedicated console servers for gstg / gprd
* [ ]  Update profile to be a database reviewer && add yourself to the team page
* [ ]  Update info on which Team Meetings you should request access to and how to ask for it (ask `@people_exp` in the `#peopleops` slack channel)
